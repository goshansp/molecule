# Molecule
This is a submodule for goshansp roles/molecule to test against Fedora CoreOS (FCOS). It's running FCOS on FCOS using Libvirt. Refer to `ansible_role_template/README.md` on how to get started.


# Known Issues
Sometimes it takes several minutes for dnsmasq to log the lease and dns/nss to become available.


# Venv activation from within role
```
$ python -m venv ~/.local/venv-molecule
$ source ~/.local/venv-molecule/bin/activate
$ pip install --upgrade pip
$ pip install -r molecule/requirements.txt
$ molecule test
```

# On Snapshots
Classical Molecule drivers do not use Snapshots. They create vms from scratch / templates. Leveraging snapshots can enable realtime (cow) restoration of molecule sandbox targets and hence is superior to prior art by speed.

A snapshot is created during `create`. The snapshot gets restored during `destroy`. Before snapshots, molecule pipelines would typically take 5 minutes. Snapshot save about 2 minutes as we're currently down to 3 minutes per pipeline. Building the cached python environment takes almost two minutes. So Molecule runtime has been reduced from 3 to 1 minute.

# Setup of Environment

## Installing Host Dependencies
```
$ rpm-ostree install gcc guestfs-tools libvirt libvirt-client libvirt-devel libvirt-nss python3-devel qemu-kvm-core -y
# grep -E '^libvirt:' /usr/lib/group >> /etc/group
```

## Updateing the QEMU image for FCOS
1. `molecule destroy`
1. Get latest url for QEMU qcow2 from https://getfedora.org/en/coreos/download?tab=metal_virtualized&stream=stable&arch=x86_64 (Virtualized, QEMU)
1. Update create.yml with qemu_image and qemu_image_url
1. Update molecule.yml with bump of platform.name to fcos-4x.
1. `molecule create` # otherwise it will fail if there is no snapshot during destroy.


# Reset VM (after Upgrade)
Via GUI: Delete VM via Virtual Machine Manager (with data) ... or ...
1. $ molecule destroy
TODO: molecule reset?
1. $ virsh list --all
1. $ virsh snapshot-list <name>
1. $ virsh snapshot-delete <name> <snapshot>
1. $ virsh undefine <name>
1. $ rm ~/.cache/molecule -rf; rm ~/.local/molecule -rf


# Making DNS work
Ensure `/etc/nsswitch.conf` entry `libvirt libvirt_guest` so fqdn will be resolved. From f37 this is done with `authselect enable-feature with-libvirt`.

Source: https://libvirt.org/nss.html

# Known Issue
```
TASK [Start molecule vm(s)] ****************************************************
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: : Transport endpoint is not connected
failed: [localhost] (item=fcos-39.hp.molecule.lab) => {"ansible_loop_var": "item", "changed": false, "item": {"disk_size": "20G", "memory_size": "8", "name": "fcos-39.hp.molecule.lab", "timezone": "Europe/Zurich", "vcpu": 8}, "msg": "/usr/libexec/qemu-bridge-helper --use-vnet --br=virbr0 --fd=26: failed to communicate with bridge helper: stderr=failed to get mtu of bridge `virbr0': No such device\n: Transport endpoint is not connected"}
```

## Solution
```
$ systemctl start libvirtd; systemctl enable libvirtd
```